<?php

/* navigation.twig */
class __TwigTemplate_0ee87cda60b664a4d199d41c4661b91e6409c3789445c7a078f7572b0c2452ef extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
    <div class=\"container\">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a class=\"navbar-brand\" href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("homepage");
        echo "\">Repositories Root</a>
        </div>
        <div class=\"navbar-collapse collapse\">
            <ul class=\"nav navbar-nav navbar-right\">
                <li><a href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("homepage");
        echo "refresh\">Refresh</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "navigation.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 10,  19 => 1,  121 => 15,  115 => 5,  105 => 23,  99 => 22,  93 => 21,  87 => 20,  81 => 19,  75 => 18,  69 => 17,  62 => 16,  60 => 15,  50 => 10,  43 => 8,  37 => 14,  31 => 6,  27 => 5,  21 => 1,  92 => 28,  90 => 27,  85 => 24,  77 => 21,  73 => 19,  67 => 17,  65 => 16,  59 => 13,  53 => 12,  49 => 10,  45 => 9,  40 => 6,  38 => 5,  35 => 4,  29 => 2,);
    }
}
