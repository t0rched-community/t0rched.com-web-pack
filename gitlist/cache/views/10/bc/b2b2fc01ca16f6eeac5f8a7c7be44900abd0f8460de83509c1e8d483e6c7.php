<?php

/* breadcrumb.twig */
class __TwigTemplate_10bcb2b2fc01ca16f6eeac5f8a7c7be44900abd0f8460de83509c1e8d483e6c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'extra' => array($this, 'block_extra'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ol class=\"breadcrumb\">
    <li><a href=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tree", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "commitishPath" => (isset($context["branch"]) ? $context["branch"] : null))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["repo"]) ? $context["repo"] : null), "html", null, true);
        echo "</a></li>
    ";
        // line 3
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 4
            echo "        <li";
            if ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last")) {
                echo " class=\"active\"";
            }
            echo ">";
            if ((!$this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last"))) {
                echo "<a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tree", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "commitishPath" => (((isset($context["branch"]) ? $context["branch"] : null) . "/") . $this->getAttribute((isset($context["breadcrumb"]) ? $context["breadcrumb"] : null), "path")))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["breadcrumb"]) ? $context["breadcrumb"] : null), "dir"), "html", null, true);
                echo "</a>";
            }
            if ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last")) {
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["breadcrumb"]) ? $context["breadcrumb"] : null), "dir"), "html", null, true);
            }
            echo "</li>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 6
        echo "
    ";
        // line 7
        $this->displayBlock('extra', $context, $blocks);
        // line 8
        echo "</ol>
";
    }

    // line 7
    public function block_extra($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "breadcrumb.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 6,  23 => 2,  20 => 1,  38 => 4,  30 => 3,  64 => 13,  53 => 11,  49 => 10,  28 => 5,  22 => 2,  19 => 1,  100 => 27,  95 => 30,  93 => 29,  90 => 28,  88 => 27,  83 => 24,  81 => 8,  78 => 22,  75 => 21,  72 => 20,  70 => 19,  55 => 14,  41 => 9,  35 => 5,  32 => 6,  29 => 3,  219 => 14,  213 => 13,  207 => 12,  203 => 10,  200 => 9,  153 => 69,  147 => 66,  142 => 64,  138 => 62,  135 => 61,  131 => 59,  126 => 56,  114 => 53,  110 => 52,  105 => 51,  102 => 49,  99 => 47,  97 => 46,  94 => 45,  91 => 44,  86 => 7,  79 => 7,  73 => 36,  67 => 14,  65 => 33,  61 => 31,  59 => 30,  48 => 21,  46 => 4,  43 => 8,  40 => 8,  37 => 7,  31 => 5,  26 => 3,);
    }
}
