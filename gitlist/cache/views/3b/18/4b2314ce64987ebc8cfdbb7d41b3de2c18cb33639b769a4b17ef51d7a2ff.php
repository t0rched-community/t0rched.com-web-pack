<?php

/* layout_page.twig */
class __TwigTemplate_3b184b2314ce64987ebc8cfdbb7d41b3de2c18cb33639b769a4b17ef51d7a2ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->env->loadTemplate("navigation.twig")->display($context);
        // line 5
        echo "
    <div class=\"container\">
        <div class=\"header row\">
            <div class=\"col-sm-12\">
                ";
        // line 9
        if (twig_in_filter((isset($context["page"]) ? $context["page"] : null), array(0 => "commits", 1 => "searchcommits"))) {
            // line 10
            echo "                <form class=\"pull-right\" action=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "basepath"), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, (isset($context["repo"]) ? $context["repo"] : null), "html", null, true);
            echo "/commits/";
            echo twig_escape_filter($this->env, (isset($context["branch"]) ? $context["branch"] : null), "html", null, true);
            echo "/search\" method=\"POST\">
                    <input type=\"search\" name=\"query\" class=\"form-control input-sm\" placeholder=\"Search commits...\">
                </form>
                ";
        } else {
            // line 14
            echo "                <form class=\"pull-right\" action=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "basepath"), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, (isset($context["repo"]) ? $context["repo"] : null), "html", null, true);
            echo "/tree/";
            echo twig_escape_filter($this->env, (isset($context["branch"]) ? $context["branch"] : null), "html", null, true);
            echo "/search\" method=\"POST\">
                    <input type=\"search\" name=\"query\" class=\"form-control input-sm\" placeholder=\"Search tree...\">
                </form>
                ";
        }
        // line 18
        echo "
                ";
        // line 19
        if (array_key_exists("branches", $context)) {
            // line 20
            echo "                    ";
            $this->env->loadTemplate("branch_menu.twig")->display($context);
            // line 21
            echo "                ";
        }
        // line 22
        echo "
                ";
        // line 23
        $this->env->loadTemplate("menu.twig")->display($context);
        // line 24
        echo "            </div>
        </div>

        ";
        // line 27
        $this->displayBlock('content', $context, $blocks);
        // line 28
        echo "
        ";
        // line 29
        $this->env->loadTemplate("footer.twig")->display($context);
        // line 30
        echo "    </div>
";
    }

    // line 27
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "layout_page.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 27,  95 => 30,  93 => 29,  90 => 28,  88 => 27,  83 => 24,  81 => 23,  78 => 22,  75 => 21,  72 => 20,  70 => 19,  55 => 14,  41 => 9,  35 => 5,  32 => 4,  29 => 3,  219 => 14,  213 => 13,  207 => 12,  203 => 10,  200 => 9,  153 => 69,  147 => 66,  142 => 64,  138 => 62,  135 => 61,  131 => 59,  126 => 56,  114 => 53,  110 => 52,  105 => 51,  102 => 49,  99 => 47,  97 => 46,  94 => 45,  91 => 44,  86 => 43,  79 => 38,  73 => 36,  67 => 18,  65 => 33,  61 => 31,  59 => 30,  48 => 21,  46 => 20,  43 => 10,  40 => 8,  37 => 7,  31 => 5,  26 => 3,);
    }
}
