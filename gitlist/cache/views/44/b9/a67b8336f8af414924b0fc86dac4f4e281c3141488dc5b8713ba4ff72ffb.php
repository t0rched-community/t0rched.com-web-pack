<?php

/* branch_menu.twig */
class __TwigTemplate_44b9a67b8336f8af414924b0fc86dac4f4e281c3141488dc5b8713ba4ff72ffb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"btn-group pull-left space-right\">
    <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\">browsing: <strong>";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["branch"]) ? $context["branch"] : null), "html", null, true);
        echo "</strong> <span class=\"caret\"></span></button>
    <ul class=\"dropdown-menu\">
        <li class=\"dropdown-header\">Branches</li>
        ";
        // line 5
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["branches"]) ? $context["branches"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 6
            echo "            <li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("branch", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "branch" => (isset($context["item"]) ? $context["item"] : null))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, (isset($context["item"]) ? $context["item"] : null), "html", null, true);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "        ";
        if ((isset($context["tags"]) ? $context["tags"] : null)) {
            // line 9
            echo "        <li class=\"dropdown-header\">Tags</li>
        ";
            // line 10
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["tags"]) ? $context["tags"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 11
                echo "            <li><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("branch", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "branch" => (isset($context["item"]) ? $context["item"] : null))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, (isset($context["item"]) ? $context["item"] : null), "html", null, true);
                echo "</a></li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "        ";
        }
        // line 14
        echo "    </ul>
</div>";
    }

    public function getTemplateName()
    {
        return "branch_menu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 13,  53 => 11,  49 => 10,  28 => 5,  22 => 2,  19 => 1,  100 => 27,  95 => 30,  93 => 29,  90 => 28,  88 => 27,  83 => 24,  81 => 23,  78 => 22,  75 => 21,  72 => 20,  70 => 19,  55 => 14,  41 => 9,  35 => 5,  32 => 6,  29 => 3,  219 => 14,  213 => 13,  207 => 12,  203 => 10,  200 => 9,  153 => 69,  147 => 66,  142 => 64,  138 => 62,  135 => 61,  131 => 59,  126 => 56,  114 => 53,  110 => 52,  105 => 51,  102 => 49,  99 => 47,  97 => 46,  94 => 45,  91 => 44,  86 => 43,  79 => 38,  73 => 36,  67 => 14,  65 => 33,  61 => 31,  59 => 30,  48 => 21,  46 => 9,  43 => 8,  40 => 8,  37 => 7,  31 => 5,  26 => 3,);
    }
}
