<?php

/* file.twig */
class __TwigTemplate_cb57dd32d6be454bd539a47f9ec77b73d3622a57b918d75c4719337502713551 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout_page.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout_page.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["page"] = "files";
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "GitList";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        $this->env->loadTemplate("breadcrumb.twig")->display(array_merge($context, array("breadcrumbs" => (isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null))));
        // line 9
        echo "
    <div class=\"source-view\">
        <div class=\"source-header\">
            <div class=\"meta\"></div>

            <div class=\"btn-group pull-right\">
                <a href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("blob_raw", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "commitishPath" => (((isset($context["branch"]) ? $context["branch"] : null) . "/") . (isset($context["file"]) ? $context["file"] : null)))), "html", null, true);
        echo "\" class=\"btn btn-default btn-sm\"><span class=\"fa fa-file\"></span> Raw</a>
                <a href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("blame", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "commitishPath" => (((isset($context["branch"]) ? $context["branch"] : null) . "/") . (isset($context["file"]) ? $context["file"] : null)))), "html", null, true);
        echo "\" class=\"btn btn-default btn-sm\"><span class=\"fa fa-bullhorn\"></span> Blame</a>
                <a href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("commits", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "commitishPath" => (((isset($context["branch"]) ? $context["branch"] : null) . "/") . (isset($context["file"]) ? $context["file"] : null)))), "html", null, true);
        echo "\" class=\"btn btn-default btn-sm\"><span class=\"fa fa-list\"></span> History</a>
            </div>
        </div>
        ";
        // line 20
        if (((isset($context["fileType"]) ? $context["fileType"] : null) == "image")) {
            // line 21
            echo "        <div class=\"text-center\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("blob_raw", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "commitishPath" => (((isset($context["branch"]) ? $context["branch"] : null) . "/") . (isset($context["file"]) ? $context["file"] : null)))), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, (isset($context["file"]) ? $context["file"] : null), "html", null, true);
            echo "\" class=\"image-blob\" /></div>

        ";
        } elseif (((isset($context["fileType"]) ? $context["fileType"] : null) == "markdown")) {
            // line 24
            echo "        <div class=\"md-view\"><div id=\"md-content\">";
            echo twig_escape_filter($this->env, (isset($context["blob"]) ? $context["blob"] : null), "html", null, true);
            echo "</div></div>

        ";
        } else {
            // line 27
            echo "        <pre id=\"sourcecode\" language=\"";
            echo twig_escape_filter($this->env, (isset($context["fileType"]) ? $context["fileType"] : null), "html", null, true);
            echo "\">";
            echo htmlentities((isset($context["blob"]) ? $context["blob"] : null));
            echo "</pre>
        ";
        }
        // line 29
        echo "    </div>

    <hr />
";
    }

    public function getTemplateName()
    {
        return "file.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 29,  83 => 27,  76 => 24,  67 => 21,  65 => 20,  59 => 17,  55 => 16,  51 => 15,  43 => 9,  40 => 8,  37 => 7,  31 => 5,  26 => 3,);
    }
}
