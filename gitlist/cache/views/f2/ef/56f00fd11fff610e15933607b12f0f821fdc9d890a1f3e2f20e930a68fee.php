<?php

/* stats.twig */
class __TwigTemplate_f2ef56f00fd11fff610e15933607b12f0f821fdc9d890a1f3e2f20e930a68fee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout_page.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout_page.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["page"] = "stats";
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "GitList";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        $this->env->loadTemplate("breadcrumb.twig")->display(array_merge($context, array("breadcrumbs" => array(0 => array("dir" => "Statistics", "path" => "")))));
        // line 9
        echo "
    <table class=\"table stats\">
        <thead>
            <tr>
                <th width=\"30%\"><span class=\"fa fa-file-text\"></span> File extensions (";
        // line 13
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["stats"]) ? $context["stats"] : null), "extensions")), "html", null, true);
        echo ")</th>
                <th width=\"40%\"><span class=\"fa fa-users\"></span> Authors (";
        // line 14
        echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["authors"]) ? $context["authors"] : null)), "html", null, true);
        echo ")</th>
                <th width=\"30%\"><span class=\"fa fa-star\"></span> Other</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <ul>
                    ";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["stats"]) ? $context["stats"] : null), "extensions"));
        foreach ($context['_seq'] as $context["ext"] => $context["amount"]) {
            // line 23
            echo "                        <li><strong>";
            echo twig_escape_filter($this->env, (isset($context["ext"]) ? $context["ext"] : null), "html", null, true);
            echo "</strong>: ";
            echo twig_escape_filter($this->env, (isset($context["amount"]) ? $context["amount"] : null), "html", null, true);
            echo " files</li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['ext'], $context['amount'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "                    </ul>
                </td>
                <td>
                    <ul>
                    ";
        // line 29
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["authors"]) ? $context["authors"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["author"]) {
            // line 30
            echo "                        <li><strong><a href=\"mailto:";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["author"]) ? $context["author"] : null), "email"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["author"]) ? $context["author"] : null), "name"), "html", null, true);
            echo "</a></strong>: ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["author"]) ? $context["author"] : null), "commits"), "html", null, true);
            echo " commits</li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['author'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "                    </ul>
                </td>
                <td>
                    <p>
                        <strong>Total files:</strong> ";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["stats"]) ? $context["stats"] : null), "files"), "html", null, true);
        echo "
                    </p>

                    <p>
                        <strong>Total bytes:</strong> ";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["stats"]) ? $context["stats"] : null), "size"), "html", null, true);
        echo " bytes (";
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (($this->getAttribute((isset($context["stats"]) ? $context["stats"] : null), "size") / 1024) / 1024)), "html", null, true);
        echo " MB)
                    </p>
                </td>
            </tr>
        </tbody>
    </table>

    <hr />
";
    }

    public function getTemplateName()
    {
        return "stats.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 40,  108 => 36,  102 => 32,  89 => 30,  85 => 29,  79 => 25,  68 => 23,  64 => 22,  53 => 14,  49 => 13,  43 => 9,  40 => 8,  37 => 7,  31 => 5,  26 => 3,);
    }
}
