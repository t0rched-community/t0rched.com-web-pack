<?php

/* menu.twig */
class __TwigTemplate_f8471d6b109962b46e61d503d92e6606ee7826a3ee8af76b20756fae4291633e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"nav nav-tabs\">
    <li";
        // line 2
        if (((isset($context["page"]) ? $context["page"] : null) == "files")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("branch", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "branch" => (isset($context["branch"]) ? $context["branch"] : null))), "html", null, true);
        echo "\">Files</a></li>
    <li";
        // line 3
        if (twig_in_filter((isset($context["page"]) ? $context["page"] : null), array(0 => "commits", 1 => "searchcommits"))) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("commits", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "commitishPath" => (isset($context["branch"]) ? $context["branch"] : null))), "html", null, true);
        echo "\">Commits</a></li>
    <li";
        // line 4
        if (((isset($context["page"]) ? $context["page"] : null) == "stats")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("stats", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "branch" => (isset($context["branch"]) ? $context["branch"] : null))), "html", null, true);
        echo "\">Stats</a></li>
  \t<li";
        // line 5
        if (((isset($context["page"]) ? $context["page"] : null) == "network")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("network", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "branch" => (isset($context["branch"]) ? $context["branch"] : null))), "html", null, true);
        echo "\">Network</a></li>
</ul>
";
    }

    public function getTemplateName()
    {
        return "menu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 4,  30 => 3,  64 => 13,  53 => 11,  49 => 10,  28 => 5,  22 => 2,  19 => 1,  100 => 27,  95 => 30,  93 => 29,  90 => 28,  88 => 27,  83 => 24,  81 => 23,  78 => 22,  75 => 21,  72 => 20,  70 => 19,  55 => 14,  41 => 9,  35 => 5,  32 => 6,  29 => 3,  219 => 14,  213 => 13,  207 => 12,  203 => 10,  200 => 9,  153 => 69,  147 => 66,  142 => 64,  138 => 62,  135 => 61,  131 => 59,  126 => 56,  114 => 53,  110 => 52,  105 => 51,  102 => 49,  99 => 47,  97 => 46,  94 => 45,  91 => 44,  86 => 43,  79 => 38,  73 => 36,  67 => 14,  65 => 33,  61 => 31,  59 => 30,  48 => 21,  46 => 5,  43 => 8,  40 => 8,  37 => 7,  31 => 5,  26 => 3,);
    }
}
