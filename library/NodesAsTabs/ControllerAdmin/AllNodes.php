<?php

class NodesAsTabs_ControllerAdmin_AllNodes extends XFCP_NodesAsTabs_ControllerAdmin_AllNodes
{
	public function actionSave()
	{
		$response = parent::actionSave();

		if ($response->redirectType == XenForo_ControllerResponse_Redirect::SUCCESS)
		{
			$nodeData = $this->_input->filter(array(
				'node_id' => XenForo_Input::UINT,
				'nat_display_tab' => XenForo_Input::UINT,
				'nat_display_tabperms' => XenForo_Input::UINT,
				'nat_tabtitle' => XenForo_Input::STRING,
				'nat_display_order' => XenForo_Input::UINT,
				'nat_position' => XenForo_Input::STRING,
				'nat_childlinks' => XenForo_Input::UINT,
				'nat_childlinksperms' => XenForo_Input::UINT,
				'nat_markread' => XenForo_Input::UINT,
				'nat_linkstemplate' => XenForo_Input::STRING,
				'nat_popup' => XenForo_Input::UINT,
				'nat_tabid' => XenForo_Input::STRING
			));

			if (empty($nodeData['node_id']))
			{
				$db = XenForo_Application::get('db');
				$nodeData['node_id'] = $db->fetchOne("
					SELECT node_id
					FROM xf_node
					ORDER BY node_id
					DESC
				");
			}

			$optionsModel = $this->_getOptionsModel();

			// $nodeData['nat_childnodes'] = $optionsModel->buildChildList($nodeData['node_id']);
			// $nodeData['nat_firstchildnodes'] = $optionsModel->buildFirstChildList($nodeData['node_id']);

			$optionsModel->saveOptions($nodeData);

			$optionsModel->deleteOrphans();
			$optionsModel->rebuildCache();
		}

		return $response;
	}

	public function actionDelete()
	{
		$response = parent::actionDelete();

		$optionsModel = $this->_getOptionsModel();
		$optionsModel->deleteOrphans();
		$optionsModel->rebuildCache();

		return $response;
	}

	public function actionValidateField()
	{
		$response = parent::actionValidateField();

		$fieldName = $this->_input->filterSingle('name', XenForo_Input::STRING);
		if (substr($fieldName, 0, 4) == 'nat_')
		{
			$newResponse = $this->_validateField('NodesAsTabs_DataWriter_Options', array(
				'existingDataKey' => $this->_input->filterSingle('node_id', XenForo_Input::UINT)
			));

			return $newResponse;
		}
		else
		{
			return $response;
		}
	}

	protected function _getOptionsModel()
	{
		return $this->getModelFromCache('NodesAsTabs_Model_Options');
	}
}