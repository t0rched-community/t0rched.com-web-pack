<?php

class NodesAsTabs_ControllerPublic_Page extends XFCP_NodesAsTabs_ControllerPublic_Page
{
	protected function _postDispatch($controllerResponse, $controllerName, $action)
	{
		parent::_postDispatch($controllerResponse, $controllerName, $action);

		$optionsModel = XenForo_Model::create('NodesAsTabs_Model_Options');
		$nodeId = (isset($controllerResponse->params['page']['node_id'])
			? $controllerResponse->params['page']['node_id']
			: 0);

		$optionsModel->postDispatch($this, $nodeId, $controllerResponse, $controllerName, $action);
	}
}