<?php

class NodesAsTabs_Model_Options extends XenForo_Model
{
	public function getAllTabs()
	{
		return $this->_getDb()->fetchAll("
			SELECT options.*, node.*
			FROM nat_options AS options
			INNER JOIN xf_node AS node
				ON (node.node_id = options.node_id)
			WHERE nat_display_tab = 1
			OR nat_tabid != ''
			ORDER BY options.nat_display_order
			ASC
		");
	}

	public function getOptionsById($nodeId)
	{
		return $this->_getDb()->fetchRow('
			SELECT *
			FROM nat_options
			WHERE node_id = ?
		', $nodeId);
	}

	public function getJoinedOptionsForNav($permComboId)
	{
		return $this->_getDb()->fetchAll("
			SELECT options.*, node.*,
				permission.cache_value AS node_permission_cache
			FROM nat_options AS options
			INNER JOIN xf_node AS node
				ON (node.node_id = options.node_id)
			LEFT JOIN xf_permission_cache_content AS permission
				ON (permission.permission_combination_id = ?
				AND permission.content_type = 'node'
				AND permission.content_id = options.node_id)
			WHERE nat_display_tab = 1
			ORDER BY options.nat_display_order
			ASC
		", $permComboId);
	}

	public function getJoinedChildrenForNav(array $nodeIds, $permComboId)
	{
		$db = $this->_getDb();

		return $db->fetchAll("
			SELECT node.*,
				permission.cache_value AS node_permission_cache
			FROM xf_node AS node
			LEFT JOIN xf_permission_cache_content AS permission
				ON (permission.permission_combination_id = ?
				AND permission.content_type = 'node'
				AND permission.content_id = node.node_id)
			WHERE node.node_id IN (" . $db->quote($nodeIds) . ")
			ORDER BY node.lft
			ASC
		", $permComboId);
	}

	public function categoryTabExists()
	{
		return $this->_getDb()->fetchOne("
			SELECT options.node_id
			FROM nat_options AS options
			LEFT JOIN xf_node AS node ON (node.node_id = options.node_id)
			WHERE node.node_type_id = 'Category'
			AND node.depth = 0
			AND options.nat_display_tab = 1
		");
	}

	public function getTabLabelsForNodeTree()
	{
		$records = $this->_getDb()->fetchAll("
			SELECT *
			FROM nat_options
			WHERE nat_display_tab = 1
			OR nat_tabid != ''
		");

		$nodeIds = array();
		foreach ($records AS $record)
		{
			$nodeIds[$record['node_id']] = $record;
		}

		return $nodeIds;
	}

	public function isTab($nodeId = 0, &$manual = '')
	{
		if (!$nodeId)
		{
			return false;
		}

		$natCache = $this->getSimpleCacheData();

		if (!isset($natCache['nodeTabs']))
		{
			return false;
		}

		$parentTab = 0;
		$depth = 0;

		foreach ($natCache['nodeTabs'] AS $nodeTab)
		{
			if ($nodeId == $nodeTab['node_id']
			OR in_array($nodeId, explode(',', $nodeTab['nat_childnodes']))
			)
			{
				if (!$parentTab OR $nodeTab['depth'] > $depth)
				{
					$depth = $nodeTab['depth'];

					if ($nodeTab['nat_display_tab'])
					{
						$parentTab = $nodeTab['node_id'];
						$manual = '';
					}
					// MANUAL TAB ASSIGNMENT
					else
					{
						// SET PARENT SO THIS BREADCRUMB ISN'T REMOVED
						// NORMALLY WOULD RETURN ID OF NODE TAB, BUT THIS IS MANUAL ASSIGNMENT
						$parentTab = ($nodeTab['parent_node_id'] ? $nodeTab['parent_node_id'] : -1);
						$manual = $nodeTab['nat_tabid'];
					}
				}
			}
		}

		return $parentTab;
	}

	public function handleRoute($nodeId, $routeMatch)
	{
		$manual = '';
		$parentTab = 0;

		// CHECK IF NODE IS IN A TAB
		if ($parentTab = $this->isTab($nodeId, $manual))
		{
			if ($manual)
			{
				// SET NEW MAJOR SECTION
				// USED INTERNALLY FOR NAV TAB SELECTION
				$routeMatch->setSections($manual);

				return $parentTab;
			}
			else
			{
				// SET NEW MAJOR SECTION
				// USED INTERNALLY FOR NAV TAB SELECTION
				$routeMatch->setSections('nodetab' . $parentTab);

				return $parentTab;
			}
		}

		return 0;
	}

	public function postDispatch($controller, $nodeId, $controllerResponse, $controllerName, $action)
	{
		$routeMatch = $controller->getRouteMatch();
		$request = $controller->getRequest();

		$nodeId = ($nodeId ? $nodeId : $this->getNodeIdFromRequest($request));

		if ($nodeTabId = $this->handleRoute($nodeId, $routeMatch))
		{
			// USED LATER FOR BREADCRUMBS
			$controllerResponse->containerParams['nodeTabId'] = $nodeTabId;
		}
	}

	public function getNodeIdFromRequest($request)
	{
		if ($nodeId = $request->getParam('node_id'))
		{
			return $nodeId;
		}
		else if ($nodeName = $request->getParam('node_name'))
		{
			$nodeId = $this->_getDb()->fetchOne("
				SELECT node_id
				FROM xf_node
				WHERE node_name = ?
			", $nodeName);

			return $nodeId;
		}
		else if ($threadId = $request->getParam('thread_id'))
		{
			$nodeId = $this->_getDb()->fetchOne("
				SELECT node_id
				FROM xf_thread
				WHERE thread_id = ?
			", $threadId);

			return $nodeId;
		}
		else if ($postId = $request->getParam('post_id'))
		{
			$nodeId = $this->_getDb()->fetchOne("
				SELECT thread.node_id
				FROM xf_post AS post
				LEFT JOIN xf_thread AS thread ON (thread.thread_id = post.thread_id)
				WHERE post.post_id = ?
			", $postId);

			return $nodeId;
		}

		return 0;
	}

	// RETURNS A COMMA LIST OF NODEIDS
	public function buildChildList($nodeId)
	{
		$list = array();

		$nodeModel = XenForo_Model::create('XenForo_Model_Node');
		$childNodes = $nodeModel->getChildNodesForNodeIds(array($nodeId));

		foreach ($childNodes AS $key => $child)
		{
			$list[] = $key;
		}
		$childList = implode(',', $list);

		return $childList;
	}

	// RETURNS NODE INFO OF FIRST CHILDREN NECESSARY FOR CREATING LINKS
	public function buildFirstChildList($nodeId)
	{
		$list = array();
		$childList = '';

		$nodeModel = XenForo_Model::create('XenForo_Model_Node');

		$curNode = $nodeModel->getNodeById($nodeId);
		$firstChildren = $nodeModel->getChildNodesToDepth($curNode, 1);

		foreach ($firstChildren AS $child)
		{
			$list[] = $child;
		}
		if (isset($list[0]))
		{
			$childList = serialize($list);
		}

		return $childList;
	}

	public function rebuildCache($nodeId = 0)
	{
		if (!$nodeId)
		{
			$nodeIds = $this->_getDb()->fetchCol("
				SELECT node_id
				FROM nat_options
				WHERE nat_display_tab = 1
				OR nat_tabid != ''
			");

			foreach ($nodeIds AS $nodeId)
			{
				$this->rebuildCache($nodeId);
			}

			$nodeTabs = $this->getAllTabs();
			$natCache = array(
				'checkTabPerms' => false,
				'nodeTabs' => $nodeTabs,
				'linkForumSelect' => array()
			);
			$db = $this->_getDb();
			foreach ($nodeTabs AS $nodeTab)
			{
				if ($nodeTab['nat_display_tabperms'] AND $nodeTab['nat_display_tab'])
				{
					$natCache['checkTabPerms'] = true;
				}

				/* LINK FORUM TAB SELECTION */
				$linkForums = $db->fetchAll("
					SELECT *
					FROM xf_link_forum AS lf
					LEFT JOIN xf_node AS n ON (n.node_id = lf.node_id)
					WHERE " . ($nodeTab['nat_childnodes'] ? "lf.node_id IN ({$nodeTab['nat_childnodes']})" : "0") . "
					OR lf.node_id = ?
				", $nodeTab['node_id']);

				foreach ($linkForums AS $linkForum)
				{
					// IF IT'S A RELATIVE TARGET
					// CONDITIONS TAKEN FROM XenForo_Link::convertUriToAbsoluteUri
					if ($linkForum['link_url'] != '.'
					AND substr($linkForum['link_url'], 0, 1) != '/'
					AND !preg_match('#^[a-z0-9-]+://#i', $linkForum['link_url'])
					)
					{
						$natCache['linkForumSelect'][$linkForum['node_id']] = $linkForum;
					}
				}
			}

			XenForo_Application::setSimpleCacheData('natCache', $natCache);
		}
		else
		{
			$nodeData = array();

			$nodeData['node_id'] = $nodeId;
			$nodeData['nat_childnodes'] = $this->buildChildList($nodeId);
			$nodeData['nat_firstchildnodes'] = $this->buildFirstChildList($nodeId);

			$this->saveOptions($nodeData);
		}
	}

	public function deleteOrphans()
	{
		$nodeIds = $this->_getDb()->fetchCol("
			SELECT options.node_id
			FROM nat_options AS options
			LEFT JOIN xf_node AS node ON (node.node_id = options.node_id)
			WHERE node.node_id IS NULL
		");

		foreach ($nodeIds AS $nodeId)
		{
			$this->deleteOptions($nodeId);
		}
	}

	public function getSimpleCacheData()
	{
		$natCache = XenForo_Application::getSimpleCacheData('natCache');

		if (!$natCache)
		{
			$this->deleteOrphans();
			$this->rebuildCache();

			$natCache = XenForo_Application::getSimpleCacheData('natCache');
		}

		return $natCache;
	}

	public function deleteOptions($nodeId)
	{
		if (!$nodeId)
		{
			return;
		}

		$existing = $this->getOptionsById($nodeId);
		if (!empty($existing['node_id']))
		{
			$dw = XenForo_DataWriter::create('NodesAsTabs_DataWriter_Options');
			$dw->setExistingData($existing['node_id']);
			$dw->delete();
		}
	}

	public function canViewNode(array $node, &$errorPhraseKey = '', array $nodePermissions = null, array $viewingUser = null)
	{
		$this->standardizeViewingUserReferenceForNode($node['node_id'], $viewingUser, $nodePermissions);

		return XenForo_Permission::hasContentPermission($nodePermissions, 'view');
	}

	public function buildLink($node)
	{
		$retval = '';

		$nodeModel = $this->getModelFromCache('XenForo_Model_Node');
		$nodeTypes = $nodeModel->getAllNodeTypes();

		if (isset($nodeTypes[$node['node_type_id']]['public_route_prefix']))
		{
			$retval = XenForo_Link::buildPublicLink('full:' . $nodeTypes[$node['node_type_id']]['public_route_prefix'], $node);
		}

		return $retval;
	}

	public function saveOptions(array $node)
	{
		if (!$node['node_id'])
		{
			return;
		}

		$dw = XenForo_DataWriter::create('NodesAsTabs_DataWriter_Options');

		$existing = $this->getOptionsById($node['node_id']);
		if (!empty($existing['node_id']))
		{
			$dw->setExistingData($existing['node_id']);
		}

		$dw->bulkSet($node);

		$dw->save();
	}
}