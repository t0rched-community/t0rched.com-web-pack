<?php

class NodesAsTabs_ViewAdmin_Node_List extends XFCP_NodesAsTabs_ViewAdmin_Node_List
{
	public function renderHtml()
	{
		$optionsModel = XenForo_Model::create('NodesAsTabs_Model_Options');
		$tabNodes = $optionsModel->getTabLabelsForNodeTree();

		foreach ($this->_params['nodes'] AS &$node)
		{
			if (!empty($tabNodes[$node['node_id']]))
			{
				if ($tabNodes[$node['node_id']]['nat_display_tab'])
				{
					$node['node_type'] .= ' - (' . new XenForo_Phrase('nat_' . $tabNodes[$node['node_id']]['nat_position'] . '_tab') . ' #' . $tabNodes[$node['node_id']]['nat_display_order'] . ')';
				}
				else if ($tabNodes[$node['node_id']]['nat_tabid'])
				{
					$node['node_type'] .= ' - (' . new XenForo_Phrase('nat_assigned_to') . ' "' . $tabNodes[$node['node_id']]['nat_tabid'] . '")';
				}
			}
		}
	}
}