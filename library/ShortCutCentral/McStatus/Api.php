<?php
/*
** Add-on by ShortCut Central | Free Support - https://www.shortcutcentral.org | Version 2.0
** Code by SCC is under: GNU Public license v3 and is provided as-is, with no warranties or liabilities
** Feel free to modify this add-on to suit your needs
*/
class ShortCutCentral_McStatus_Api {
	public static function getXenCallback()
	{
		if(func_get_arg(1)){
			$mode = func_get_arg(1);
		} else {
			$mode = 'HTML';
		}
		//$xenOptions = XenForo_Application::get('options');
		if($mode == 'HTML')
		{
			echo self::getStatusHTML();
		} else {
			return self::getStatusHTML(false);
		}
	}

	public static function getStatusHTML($html = true)
	{
		require_once(__DIR__.'/View.php');
		$view = new ShortCutCentral_McStatus_View();
		$o = XenForo_Application::get('options');
		$options = array(
			'IP'    => $o->SCC_McStatus_IP,
			'DIP'   => $o->SCC_McStatus_DIP,
			'Mode'  => $o->SCC_McStatus_Mode
		);

		if(strpos($options['IP'], ':'))
		{
			$optionsIP = explode(':', $options['IP']);
			$options['IP'] = $optionsIP[0];
			$options['Port'] = (int) $optionsIP[1];
		} else {
			$options['Port'] = 25565;
		}

		return $view->getData($options, $html);
	}
}
