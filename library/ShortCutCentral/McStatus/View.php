<?php
/*
** Add-on by ShortCut Central | Free Support - https://www.shortcutcentral.org | Version 2.0
** Code by SCC is under: GNU Public license v3 and is provided as-is, with no warranties or liabilities
** Feel free to modify this add-on to suit your needs
*/

class ShortCutCentral_McStatus_View {
	/**
	 * Main class, works with the default xenForo sidebar to add server status
	 * Use $template->getParams() to pass to getOptions()
	 */
	public static function renderStatus($templateName, &$contents, array &$containerData, XenForo_Template_Abstract $template)
	{
		if($templateName != 'forum_list')
		{
			return;
		}

		$params = $template->getParams();
		if($options = self::getOptions($params['xenOptions']))
		{
			$data = self::getData($options, true);
			$containerData['sidebar'] = str_replace('<!-- SCC Mc Status -->', $data, $containerData['sidebar']);
		}

		return;
	}

	/**
	 * Retrieves the options/phrases for use with the add-on.
	 * Retrieves all options that start with SCC_McStatus_
	 */
	public static function getOptions(array $params)
	{
		foreach($params as $key => $value)
		{
			if(substr($key, 0, 13) == 'SCC_McStatus_'){
				 $options[substr($key, 13)] = $value;
			}
		}

		// No options found, or IP not found
		if(!$options || $options['IP'] == '' || $options['IP'] == null)
		{
			return false;
		}

		// Display IP empty, set value of IP
		if($options['DIP'] == '' || !$options['DIP'])
		{
			$options['DIP'] = $options['IP'];
		}

		//If user included a port, separate the port from IP
		if(strpos($options['IP'], ':'))
		{
			$optionsIP = explode(':', $options['IP']);
			$options['IP'] = $optionsIP[0];
			$options['Port'] = (int) $optionsIP[1];
		} else {
			$options['Port'] = 25565;
		}

		return $options;
	}

	/**
	 * If $html = null, then it'll return an array with the raw details
	 * If $html are passed to function, it'll return a variable with sidebar HTML
	 * to be added anywhere on the front end.
	 */
	public static function getData(array $options, $html = false)
	{
		if($options['Mode'] == 'full')
		{
			require_once( __DIR__ . '/Query.php');
			$query = new Query();
			if(!$status = $query->getStatus($options['IP'], $options['Port'])){
				return self::renderSidebar(false, $options, $html);
			}
		} elseif($options['Mode'] == 'mcapi' || $options['Mode'] == 'devro') {
			if($options['Mode'] == 'mcapi'){
				$status = json_decode(file_get_contents('https://mcapi.ca/query/'.$options['IP'].':'.$options['Port'].'/info'), true);

				// Handle offline servers
				if($status['status'] == false){
					return self::renderSidebar(false, $options, $html);
				}
				$status['favicon'] = 'https://mcapi.ca/query/'.$options['IP'].':'.$options['Port'].'/icon';
			} else {
				$status = json_decode(file_get_contents('https://devro.me/api/server/'.$options['IP'].'/players/'), true);

				// Handle offline servers
				if(isset($status['error'])){
					return self::renderSidebar(false, $options, $html);
				}
				$status['favicon'] = 'https://devro.me/api/server/'.$options['IP'].'/icon/';
				// Poor DevRo, no support for these variables.
				$status['ping'] = 'Unavailable';
				$status['motd'] = 'Unavailable';
				$status['version'] = 'Unavailable';
			}
			// Adjusting the variable names to match integrated variables
			$status['onlineplayers'] = $status['players']['online'];
			$status['maxplayers'] = $status['players']['max'];
			$status['motd_raw'] = $status['motd'];
			// Erase variables
			$status['players'] = null;
			$status['motd'] = null;
		}
		return self::renderSidebar($status, $options, $html);
	}

	private static function renderSidebar($status, $options, $html = true)
	{
		if($html == false){
			return $status;
		}
		/**
		** Begin building sidebar HTML
		** Arrays are created to designate strings to be replaced through the options.
		*/
		$publicArray = array('IP', 'DIP');
		$statusArray = array('onlineplayers', 'maxplayers', 'version', 'motd_raw', 'ping', 'favicon');

		if($status){
			$template = new XenForo_Template_Public('SCC_McStatus_Online');
			$template->addRequiredExternal('css', 'SCC_McStatus_Style');
			$base = $template->render();
			foreach($statusArray as $value){
				$base = str_replace('[['.$value.']]', $status[$value], $base);
			}
		} else {
			$template = new XenForo_Template_Public('SCC_McStatus_Offline');
			$template->addRequiredExternal('css', 'SCC_McStatus_Style');
			$base = $template->render();
		}

		foreach($publicArray as $value){
		$base = str_replace('[['.$value.']]', $options[$value], $base);
		}

		if(strpos($base, '[[motd]]') && $options['Mode'] == 'full'){
		$base = str_replace('[[motd]]', self::prettifyMOTD($status['motd_raw']), $base);
		}
		return $base;
	}

	// MOTD prettifier
	// Credits to Tv_Controls_you on Minecraftforum.net | http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-tools/1264944
	private static function prettifyMOTD($raw){
		preg_match_all("/[^§&]*[^§&]|[§&][0-9a-z][^§&]*/", $raw, $brokenupstrings);
		$returnstring = "";
		foreach ($brokenupstrings as $results) {
			$ending = '';
			foreach ($results as $individual) {
				$code = preg_split("/[&§][0-9a-z]/", $individual);
				preg_match("/[&§][0-9a-z]/", $individual, $prefix);
				if (isset($prefix[0])) {
					$actualcode = substr($prefix[0], 1);
					switch ($actualcode) {
						case "1":
							$returnstring = $returnstring . '<FONT COLOR="0000AA">';
							$ending       = $ending . "</FONT>";
							break;
						case "2":
							$returnstring = $returnstring . '<FONT COLOR="00AA00">';
							$ending       = $ending . "</FONT>";
							break;
						case "3":
							$returnstring = $returnstring . '<FONT COLOR="00AAAA">';
							$ending       = $ending . "</FONT>";
							break;
						case "4":
							$returnstring = $returnstring . '<FONT COLOR="AA0000">';
							$ending       = $ending . "</FONT>";
							break;
						case "5":
							$returnstring = $returnstring . '<FONT COLOR="AA00AA">';
							$ending       = $ending . "</FONT>";
							break;
						case "6":
							$returnstring = $returnstring . '<FONT COLOR="FFAA00">';
							$ending       = $ending . "</FONT>";
							break;
						case "7":
							$returnstring = $returnstring . '<FONT COLOR="AAAAAA">';
							$ending       = $ending . "</FONT>";
							break;
						case "8":
							$returnstring = $returnstring . '<FONT COLOR="555555">';
							$ending       = $ending . "</FONT>";
							break;
						case "9":
							$returnstring = $returnstring . '<FONT COLOR="5555FF">';
							$ending       = $ending . "</FONT>";
							break;
						case "a":
							$returnstring = $returnstring . '<FONT COLOR="55FF55">';
							$ending       = $ending . "</FONT>";
							break;
						case "b":
							$returnstring = $returnstring . '<FONT COLOR="55FFFF">';
							$ending       = $ending . "</FONT>";
							break;
						case "c":
							$returnstring = $returnstring . '<FONT COLOR="FF5555">';
							$ending       = $ending . "</FONT>";
							break;
						case "d":
							$returnstring = $returnstring . '<FONT COLOR="FF55FF">';
							$ending       = $ending . "</FONT>";
							break;
						case "e":
							$returnstring = $returnstring . '<FONT COLOR="FFFF55">';
							$ending       = $ending . "</FONT>";
							break;
						case "f":
							$returnstring = $returnstring . '<FONT COLOR="FFFFFF">';
							$ending       = $ending . "</FONT>";
							break;
						case "l":
							if (strlen($individual) > 2) {
								$returnstring = $returnstring . '<span style="font-weight:bold;">';
								$ending       = "</span>" . $ending;
								break;
							}
						case "m":
							if (strlen($individual) > 2) {
								$returnstring = $returnstring . '<strike>';
								$ending       = "</strike>" . $ending;
								break;
							}
						case "n":
							if (strlen($individual) > 2) {
								$returnstring = $returnstring . '<span style="text-decoration: underline;">';
								$ending       = "</span>" . $ending;
								break;
							}
						case "o":
							if (strlen($individual) > 2) {
								$returnstring = $returnstring . '<i>';
								$ending       = "</i>" . $ending;
								break;
							}
						case "r":
							$returnstring = $returnstring . $ending;
							$ending       = '';
							break;
					}
					if (isset($code[1])) {
						$returnstring = $returnstring . $code[1];
						if (isset($ending) && strlen($individual) > 2) {
							$returnstring = $returnstring . $ending;
							$ending       = '';
						}
					}
				} else {
					$returnstring = $returnstring . $individual;
				}

			}
		}
		return $returnstring;
	}

}
