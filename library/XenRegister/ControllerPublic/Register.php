<?php
class XenRegister_ControllerPublic_Register extends XFCP_XenRegister_ControllerPublic_Register {

	protected function _completeRegistration(array $user, array $extraParams = array()) {

		$visitor = XenForo_Visitor::setup($user['user_id']);
		$options = XenForo_Application::get('options');

		foreach($options->server_ips as $k => $v) {
			$sendData = hash('sha256', $visitor['username'] . $v['password']);
			$sendData .= "-" . $visitor['username'];

			$fp = fsockopen($v['ip'], $v['port'], $errno, $errstr, 5);

			if($fp) {
				fwrite($fp, $sendData);
			}
			
			fclose($fp);
		}

		return parent::_completeRegistration($user, $extraParams);
	}

}