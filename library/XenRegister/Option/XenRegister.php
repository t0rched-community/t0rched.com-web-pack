<?php
class XenRegister_Option_XenRegister {

	public static function renderOption(XenForo_View $view, $fieldPrefix, array $preparedOption, $canEdit) {
		$value = $preparedOption['option_value'];

		$choices = array();
		foreach ($value as $k => $v) {
			$choices[] = array(
				'ip' => $v['ip'],
				'port' => (is_numeric($v['port']) ? $v['port'] : ($v['port'] == '' ? '' : 8384)),
				'password' => $v['password']
			);
		}

		$editLink = $view->createTemplateObject('option_list_option_editlink', array(
			'preparedOption' => $preparedOption,
			'canEditOptionDefinition' => $canEdit
		));

		return $view->createTemplateObject('option_template_xenregister_ip', array(
			'fieldPrefix' => $fieldPrefix,
			'listedFieldName' => $fieldPrefix . '_listed[]',
			'preparedOption' => $preparedOption,
			'formatParams' => $preparedOption['formatParams'],
			'editLink' => $editLink,

			'choices' => $choices,
			'nextCounter' => count($choices)
		));
	}

	public static function verifyOption(array &$values, XenForo_DataWriter $dw, $fieldName) {
		
		$output = array();

		foreach($values as $k => $v) {
			if($v['ip'] != '' || $v['port'] != '' || $v['password'] != '') {
				$output[] = array(
						'ip' => $v['ip'],
						'port' => $v['port'],
						'password' => $v['password']
					);
			}
		}

		$values = $output;

		return true;
	}

}