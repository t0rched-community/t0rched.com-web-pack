#!/bin/bash
cd "/t0rched.com-mod-pack/t0rched.com Community Mod Pack/src/mods";
rm /web/t0rched.com/modlist.txt
find . -type f -follow -print > /web/t0rched.com/modlist.txt
sed -i -e 's/\.\///g' /web/t0rched.com/modlist.txt
sed -i -e 's/ic2//g' /web/t0rched.com/modlist.txt
sed -i -e 's/1.7.10//g' /web/t0rched.com/modlist.txt
sed -i -e 's/\///g' /web/t0rched.com/modlist.txt
sed -i -e 's/\[\]//g' /web/t0rched.com/modlist.txt
sed -i -e 's/ //g' /web/t0rched.com/modlist.txt
sed -i -e 's/\_.jar//g' /web/t0rched.com/modlist.txt
sed -i -e 's/\.jar//g' /web/t0rched.com/modlist.txt
sed -i -e '/carpentersblocksCarpentersBlocksCachedResources\.zip/d' /web/t0rched.com/modlist.txt
sed -i -e '/PortalGunSounds\.pak/d' /web/t0rched.com/modlist.txt
sed -i -e 's/_\-/-/g' /web/t0rched.com/modlist.txt
sed -i -e 's/\-_/_/g' /web/t0rched.com/modlist.txt
sed -i -e 's/__/_/g' /web/t0rched.com/modlist.txt
sed -i -e 's/_\./\./g' /web/t0rched.com/modlist.txt
sed -i -e 's/\-\-/\-/g' /web/t0rched.com/modlist.txt
sort -o /web/t0rched.com/modlist.txt /web/t0rched.com/modlist.txt
DATE=`date +%Y-%m-%d`
TIME=`date +%T%p`
sed -i "1s/^/\\n<div style\=font\-size\:2em\;font-weight\:bold\;\>This List was Updated on $DATE at $TIME\<\/div\>\n/" /web/t0rched.com/modlist.txt
chmod -R 777 /web/t0rched.com/modlist.txt
